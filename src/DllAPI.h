#pragma once

#ifdef QT_WINDOWS_PLATFORM
	#define QT_WINDOWS_PLATFORM_API __declspec(dllexport)
#else
	#define QT_WINDOWS_PLATFORM_API __declspec(dllimport)
#endif