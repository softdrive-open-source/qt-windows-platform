// Copyright (C) 2022 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only

#ifndef SWINDOWSMIMECONVERTER_P_H
#define SWINDOWSMIMECONVERTER_P_H

#include <QtGui/qtguiglobal.h>

struct tagFORMATETC;
using FORMATETC = tagFORMATETC;
struct tagSTGMEDIUM;
using STGMEDIUM = tagSTGMEDIUM;
struct IDataObject;

QT_BEGIN_NAMESPACE

class QMetaType;
class QMimeData;
class QVariant;

// Softdrive implementation of QWindowsMimeConverter that calls the QWindowsMimeRegistry directly instead of going through the QGuiApplication
// We do this because otherwise we leave the dll and end up in qt's code instead of this dll.
class SWindowsMimeConverter
{
    Q_DISABLE_COPY(SWindowsMimeConverter)
public:
    SWindowsMimeConverter();
    virtual ~SWindowsMimeConverter();

    static int registerMimeType(const QString &mimeType);

    // for converting from Qt
    virtual bool canConvertFromMime(const FORMATETC &formatetc, const QMimeData *mimeData) const = 0;
    virtual bool convertFromMime(const FORMATETC &formatetc, const QMimeData *mimeData, STGMEDIUM * pmedium) const = 0;
    virtual QList<FORMATETC> formatsForMime(const QString &mimeType, const QMimeData *mimeData) const = 0;

    // for converting to Qt
    virtual bool canConvertToMime(const QString &mimeType, IDataObject *pDataObj) const = 0;
    virtual QVariant convertToMime(const QString &mimeType, IDataObject *pDataObj, QMetaType preferredType) const = 0;
    virtual QString mimeForFormat(const FORMATETC &formatetc) const = 0;
};

QT_END_NAMESPACE

#endif // SWINDOWSMIMECONVERTER_P_H
