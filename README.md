# Qt windows platform

This is an heavily trimmed down version of the qt windows platform. It contains only the code necessary to make QWindowsOleDataObject work.  
QWindowsOleDataObject is also now exported in a dll.  

The current code is based on Qt 6.4.

## Authors and acknowledgment
Based on https://github.com/qt/qtbase. 

## License
Licensed according to Qt's LGPL license. For more details, see Licenses/Qt.