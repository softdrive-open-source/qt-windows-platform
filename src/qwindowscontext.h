// Copyright (C) 2016 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only

#ifndef QWINDOWSCONTEXT_H
#define QWINDOWSCONTEXT_H

#include <QtCore/qloggingcategory.h>
#include <QtCore/qscopedpointer.h>
#include <QtCore/qsharedpointer.h>
#include "qwindowsmimeregistry.h"

#define STRICT_TYPED_ITEMIDS
#include <shlobj.h>
#include <shlwapi.h>

QT_BEGIN_NAMESPACE

class QWindow;
class QPlatformScreen;
class QPlatformWindow;
class QWindowsMenuBar;
class QWindowsScreenManager;
class QWindowsTabletSupport;
class QWindowsWindow;
class QWindowsMimeConverter;
struct QWindowCreationContext;
struct QWindowsContextPrivate;
class QPoint;
class QKeyEvent;
class QPointingDevice;

class QWindowsContext
{
	Q_DISABLE_COPY_MOVE(QWindowsContext)
public:
	// Verbose flag set by environment variable QT_QPA_VERBOSE
	static int verbose;

	explicit QWindowsContext();
	~QWindowsContext();

	static QWindowsContext* instance();

	QWindowsMimeRegistry &mimeConverter() const;

	QScopedPointer<QWindowsContextPrivate> d;
	static QWindowsContext* m_instance;
};

QT_END_NAMESPACE

#endif	// QWINDOWSCONTEXT_H
