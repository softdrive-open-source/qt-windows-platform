// Copyright (C) 2013 Samuel Gaist <samuel.gaist@edeltech.ch>
// Copyright (C) 2016 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only

#include "qwindowscontext.h"

#include "qwindowsmimeconverter.h"

#include <QtCore/private/qfactorycacheregistration_p.h>
#include <QtCore/private/qwinregistry_p.h>
#include <QtCore/qdebug.h>
#include <QtCore/qscopedpointer.h>
#include <QtCore/quuid.h>
#include <QtGui/qopenglcontext.h>
#include <private/qguiapplication_p.h>

#include <comdef.h>
#include <shellscalingapi.h>
#include <stdio.h>

QT_BEGIN_NAMESPACE

int QWindowsContext::verbose = 0;

QWindowsContext* QWindowsContext::m_instance = nullptr;

/*!
	\class QWindowsContext
	\brief Singleton container for all relevant information.

	Holds state information formerly stored in \c qapplication_win.cpp.

	\internal
*/

typedef QHash<HWND, QWindowsWindow*> HandleBaseWindowHash;

struct QWindowsContextPrivate
{
	QWindowsContextPrivate();

	QWindowsMimeRegistry m_mimeConverter;
	const HRESULT m_oleInitializeResult;
};

QWindowsContextPrivate::QWindowsContextPrivate()
	: m_oleInitializeResult(OleInitialize(nullptr))
{
}

QWindowsContext::QWindowsContext()
	: d(new QWindowsContextPrivate)
{
}

QWindowsContext::~QWindowsContext()
{
	m_instance = nullptr;
}

QWindowsContext* QWindowsContext::instance()
{
	if (m_instance == nullptr)
	{
		m_instance = new QWindowsContext();
	}
	return m_instance;
}

QWindowsMimeRegistry& QWindowsContext::mimeConverter() const
{
	return d->m_mimeConverter;
}
QT_END_NAMESPACE
